@extends('layouts.app')

@section('content')
<div class="container" id = "user-index">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Users
                    <a href="{{ route('user.create')}}" type="button" class="btn btn-primary" style="float: right">Add User</a>
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Access</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $item)
                            <tr>
                                <th scope="row">{{ $item->id }}</th>
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->email }}</td>
                                <td>
                                    {{ $item->access->name ?? '-' }}
                                </td>
                                <td>
                                    <a href="{{ route('user.edit', ['user'=> $item->id]  )}}" type="button" class="btn btn-success">Edit</a>
                                    <a href="{{ route('user.show', $item->id )}}" type="button" class="btn btn-secondary">Show</a>
                                    
                                    <form action="{{ route('user.update', $item->id) }}" method="POST">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>                               
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
