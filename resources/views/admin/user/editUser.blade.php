@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Edit User
                    
                </div>
                <div class="card-body">
                    
                <form action="{{ route('user.update', $Users->id )}}" method="post">
                    @csrf
                    @method('PUT')
                    <div class="mb-3">
                        <label class="form-label">Name</label>
                        <input type="text" class="form-control" name="name" value="{{ $Users->name }}">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Email</label>
                        <input type="email" class="form-control" name="email" value="{{ $Users->email }}">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Access</label>
                        <select name="access_id">
                            @foreach ($accesses as $item )
                            <option {{ $item->id == $Users->access_id ? "selected" : "" }} value="{{ $item->id }}">{{ $item->name }}</option>    
                            @endforeach
                            <!-- 
                            <option value="1">Owner</option>    
                            <option value="2">Admin</option>    
                            <option value="1">Member</option>     -->
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
