@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Add Voucher
                    
                </div>
                <div class="card-body">
                    
                <form action="{{ route('voucher.store')}}" method="POST">
                    @csrf
                    <div class="mb-3">
                        <label class="form-label">ID</label>
                        <input type="text" class="form-control", name="id">
                        @if ($errors->has('id'))
                            <span>{{ $errors->first('id') }}</span>
                        @endif
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Voucher Code</label>
                        <input type="text" class="form-control", name="voucher_code">
                        @if ($errors->has('voucher_code'))
                            <span>{{ $errors->first('voucher_code') }}</span>
                        @endif
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Discount Amount</label>
                        <input type="bigInteger" class="form-control", name="discount_amt">
                        @if ($errors->has('discount_amt'))
                            <span>{{ $errors->first('discount_amt') }}</span>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection