@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Edit Product
                    
                </div>
                <div class="card-body">
                    
                <form action="{{ route('product.update', $Products->id )}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="mb-3">
                        <label class="form-label">Product Name</label>
                        <input type="text" class="form-control" name="item" value="{{ $Products->item }}">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Product Description</label>
                        <input type="text" class="form-control" name="description" value="{{ $Products->description }}">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Stock</label>
                        <input type="integer" class="form-control" name="qty" value="{{ $Products->qty }}">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Unit Price</label>
                        <input type="integer" class="form-control" name="price" value="{{ $Products->price }}">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Product Image</label>
                        <input type="text" class="form-control" name="image_url" value="{{ $Products->image_url }}">
                    </div>
                        
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
