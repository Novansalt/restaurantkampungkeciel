@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Add New Product
                </div>
                <div class="card-body">
                    
                <form action="{{ route('product.store')}}" method="POST">
                    @csrf
                    <div class="mb-3">
                        <label class="form-label">Product Name</label>
                        <input type="text" class="form-control" name="item">
                        @if ($errors->has('item'))
                            <span>{{ $errors->first('item') }}</span>
                        @endif
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Product Description</label>
                        <input type="text" class="form-control" name="description">
                        @if ($errors->has('description'))
                            <span>{{ $errors->first('description') }}</span>
                        @endif
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Stock</label>
                        <input type="integer" class="form-control" name="qty">
                        @if ($errors->has('qty'))
                            <span>{{ $errors->first('qty') }}</span>
                        @endif
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Unit Price</label>
                        <input type="integer" class="form-control" name="price">
                        @if ($errors->has('price'))
                            <span>{{ $errors->first('price') }}</span>
                        @endif
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Image Url</label>
                        <input type="text" class="form-control" name="image_url">
                        @if ($errors->has('image_url'))
                            <span>{{ $errors->first('image_url') }}</span>
                        @endif
                    </div>
                
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection