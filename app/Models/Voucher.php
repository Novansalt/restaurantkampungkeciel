<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'id', 'voucher_code','discount_amt'
    ];

    public function reserve()
    {
        return $this->belongsTo(Order::class);
    }
}
