<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Voucher;
use App\Models\Access;
use App\Http\Requests\VoucherValidationRequest;

class VoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $vouchers = Voucher::all();
        return view('admin.voucher.index', compact('vouchers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vouchers = Voucher::all();
        return view('admin.voucher.addVoucher');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VoucherValidationRequest $request)
    {
        $validated = $request->validated();

        $isExist = Voucher::where('voucher_code', $request->voucher_code)->first();
        if($isExist)
        {
            echo'Voucher Sudah Tersedia';
        }else{
            $model = new Voucher();
            $model->voucher_code = $request->voucher_code;
            $model->discount_amt = $request->discount_amt;
            
            if($model->save())
            {
                return redirect(route('voucher.index'));
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $hasAccessUsers = $this->checkAccess();

        $vouchers = Voucher::findOrFail($id);
    
        return view('admin.voucher.showVoucher', compact('vouchers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $hasAccessUsers = $this->checkAccess();

        $accesses = Access::all();
        $vouchers = Voucher::findOrFail($id);
    
        return view('admin.voucher.editVoucher', compact('vouchers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $hasAccessUsers = $this->checkAccess();
        $vouchers = Voucher::findOrFail($id);

        $vouchers->voucher_code = $request->voucher_code;
        $vouchers->discount_amt = $request->discount_amt;
       
        $vouchers->save();
        return redirect(route('voucher.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hasAccessUsers = $this->checkAccess();
        $vouchers = Voucher::findOrFail($id);
        if($vouchers->delete()){
            return redirect(route('voucher.index'));
        }
    }

    private function checkAccess()
    {
        $user = auth()->user();
        $accessId = $user->access_id;

        $redirect = 'Drupal';
        if($accessId === 1)
        {
            $redirect = 'menu admin';
        }
    }
}
